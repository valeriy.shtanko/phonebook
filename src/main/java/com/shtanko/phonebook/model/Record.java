package com.shtanko.phonebook.model;

import java.io.Serializable;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 6:13 PM
 */
public class Record  implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String phone;

    public Record() {
    }

    public Record(Record source) {
        this.id    = source.id;
        this.name  = source.name;
        this.phone = source.phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id='"      + id    + '\'' +
                ", name='"  + name  + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}

package com.shtanko.phonebook.db;

import com.shtanko.phonebook.model.Record;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 6:12 PM
 */
public class Database {
    private static Database instance;
    private static Map<String, DbRecord> data    = new HashMap<String, DbRecord>(100);
    private static Map<String, DbRecord> journal = new HashMap<String, DbRecord>(100);

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }

        return instance;
    }

    //------------------------------------------------------------------------------------------------------------------
    // CRUD Operations
    //
    public void addRecord(Record record) throws DbException  {
        validateRecord(record);

        if (!containsKey(record.getId())) {
            DbRecord dbRecord = new DbRecord(record);

            dbRecord.setState(RecordState.INSERTED);

            data.put(dbRecord.getId(), dbRecord);
        } else {
            throw new DbException("Record with id '%s' already exists", record.getId());
        }
    }

    public void updateRecord(Record record)  throws DbException {
        validateRecord(record);

        if (containsKey(record.getId())) {
            DbRecord originalRecord = internalGetRecord(record.getId()); // save previous data
            DbRecord newRecord      = new DbRecord(record);

            if ((originalRecord.getState() != RecordState.INSERTED)) {
                newRecord.setState(RecordState.UPDATED);
                originalRecord.setState(RecordState.UPDATED);

                saveToJournal(originalRecord);
            } else {
                newRecord.setState(RecordState.INSERTED);
            }

            data.put(newRecord.getId(), newRecord);
        } else {
            throw new DbException("Record with id '%s' not found", record.getId());
        }
    }

    public void deleteRecord (Record record) throws DbException  {
        validateRecord(record);

        deleteRecord(record.getId());
    }

    public void deleteRecord (String id) throws DbException {
        if (containsKey(id)) {
            DbRecord dbRecord = internalGetRecord(id);

            if (dbRecord.getState() != RecordState.INSERTED) {
                dbRecord.setState(RecordState.DELETED);

                saveToJournal(dbRecord);
            }

            data.remove(id);
        }
    }

    public Record getRecord(String id) throws DbException {
        DbRecord dbRecord = internalGetRecord(id);

        return  dbRecord != null ? new Record(dbRecord)
                                 : null;
    }

    public List<Record> getAllRecords() {
        List <Record> result = new ArrayList<Record>(data.size());

        for (DbRecord record : data.values()) {
            result.add(new Record(record));
        }

        return result;
    }

    public DbStatistics getStatistic() {
        int inserted = 0;
        int updated  = 0;

        for (DbRecord record : data.values()) {
            switch (record.getState()) {
                case INSERTED: inserted++; break;
                case UPDATED : updated++;  break;
            }
        }

        int deleted  = 0;

        for (DbRecord record : journal.values()) {
           if (record.getState() == RecordState.DELETED) {
               deleted++;
           }
        }

        DbStatistics result = new DbStatistics();

        result.setDeletedQuantity(deleted);
        result.setInsertedQuantity(inserted);
        result.setUpdatedQuantity(updated);
        result.setTotalQuantity(data.size());

        return result;
    }

    public int commit() {
        int result = 0;

        for (DbRecord record : data.values()) {
            if (record.getState() != RecordState.COMMITTED) {
                record.setState(RecordState.COMMITTED);

                result ++;
            }
        }

        journal.clear();

        return result;
    }

    public int rollback() {
        int result = journal.size();

        for (DbRecord record : journal.values()) {
            record.setState(RecordState.COMMITTED);
            data.put(record.getId(), record);
        }

        journal.clear();

        return result;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Protected
    //
    private void validateRecord(Record record)  throws DbException {
        if (record.getId() == null) {
            throw new DbException("Record id not defined");
        }
    }

    private boolean containsKey(String id) {
        return data.containsKey(id);
    }

    private DbRecord internalGetRecord(String id) {
      return data.get(id);
    }

    private void saveToJournal(DbRecord record) {
        // don't save uncommitted records to journal
        if (record.getState() == RecordState.INSERTED) {
            return;
        }

        // don't override previous data
        if (journal.containsKey(record.getId())) {
          return;
        }

        // Create copy
        DbRecord journalRecord = new DbRecord(record);

        // But save state (for statistics)
        journalRecord.setState(record.getState());

        journal.put(journalRecord.getId(), journalRecord);
    }
}

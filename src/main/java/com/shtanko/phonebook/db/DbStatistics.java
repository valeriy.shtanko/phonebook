package com.shtanko.phonebook.db;

/**
 * User: valeriy.shtanko
 * Date: 2/11/14
 * Time: 9:27 PM
 */
public class DbStatistics {
    private int insertedQuantity;
    private int updatedQuantity;
    private int deletedQuantity;
    private int totalQuantity;

    public int getInsertedQuantity() {
        return insertedQuantity;
    }

    public void setInsertedQuantity(int insertedQuantity) {
        this.insertedQuantity = insertedQuantity;
    }

    public int getUpdatedQuantity() {
        return updatedQuantity;
    }

    public void setUpdatedQuantity(int updatedQuantity) {
        this.updatedQuantity = updatedQuantity;
    }

    public int getDeletedQuantity() {
        return deletedQuantity;
    }

    public void setDeletedQuantity(int deletedQuantity) {
        this.deletedQuantity = deletedQuantity;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public int getNotCommitedQuantity() {
        return  insertedQuantity + updatedQuantity + deletedQuantity;
    }
}

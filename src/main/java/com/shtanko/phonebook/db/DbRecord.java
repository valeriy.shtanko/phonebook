package com.shtanko.phonebook.db;

import com.shtanko.phonebook.model.Record;

/**
 * @author: valeriy.shtanko
 * created: 2014-02-12 10:07
 */
public class DbRecord extends Record {
    private transient RecordState state = RecordState.UNDEFINED;

    public DbRecord(Record source) {
        setId(source.getId());
        setName(source.getName());
        setPhone(source.getPhone());
    }

    public RecordState getState() {
        return state;
    }

    public void setState(RecordState state) {
        this.state = state;
    }

}

package com.shtanko.phonebook.db;

/**
 * Date: 2/10/14
 * Time: 11:07 PM
 */
public enum RecordState {
    UNDEFINED,
    COMMITTED,
    INSERTED,
    UPDATED,
    DELETED
}

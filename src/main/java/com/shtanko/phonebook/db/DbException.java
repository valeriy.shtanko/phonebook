package com.shtanko.phonebook.db;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 6:56 PM
 */
public class DbException extends Exception {
    public DbException(String message) {
        super (message);
    }

    public DbException(String format, Object... args) {
        super (String.format(format, args));
    }
}

package com.shtanko.phonebook.command;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:36 PM
 */
public class CommandParser {
    private static CommandParser instance;

    public static CommandParser getInstance() {
        if (instance == null) {
            instance = new CommandParser();
        }

        return instance;
    }

    public Command parse(String commandText) throws CommandException {
        List<String> params= Arrays.asList(Pattern.compile(" -").split(commandText.trim()));

        Command command = getCommand (params.get(0));

        if (command == null) {
            throw new CommandException("Unknown command '%s'", params.get(0));
        }

        Map<String, String> commandParams = new HashMap<String, String>(10);

        for (int i = 1; i < params.size(); i++) {
            parseParam(params.get(i),commandParams);
        }

        command.setParameters(commandParams);

        return command;
    }

    //------------------------------------------------------------------------------------------------------------------
    // Protected
    //
    private void parseParam(String value, Map<String,String> storage) {
        List<String> param = Arrays.asList(Pattern.compile("=").split(value.trim()));

        String paramKey   = param.get(0).trim().toUpperCase();
        String paramValue = param.get(1).trim();

        storage.put(paramKey, paramValue);
    }

    private Command getCommand (String value) {
        Command result = null;

        value = value.trim().toUpperCase();

        if (value.equals(Command.COMMAND_ADD)) {
            result = new AddCommand();
        } else
        if (value.equals(Command.COMMAND_UPDATE)) {
            result = new UpdateCommand();
        } else
        if (value.equals(Command.COMMAND_READ)) {
            result = new ReadCommand();
        } else
        if (value.equals(Command.COMMAND_DELETE)) {
            result = new DeleteCommand();
        } else
        if (value.equals(Command.COMMAND_FLUSH)) {
            result = new FlushCommand();
        } else
        if (value.equals(Command.COMMAND_LOAD)) {
            result = new LoadCommand();
        } else
        if (value.equals(Command.COMMAND_STAT)) {
            result = new StatCommand();
        } else
        if (value.equals(Command.COMMAND_COMMIT)) {
            result = new CommitCommand();
        } else
        if (value.equals(Command.COMMAND_ROLLBACK)) {
            result = new RollbackCommand();
        }

        return result;
    }
}

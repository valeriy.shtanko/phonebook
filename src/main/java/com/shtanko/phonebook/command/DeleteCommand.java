package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:25 PM
 */
public class DeleteCommand extends Command {
    @Override
    public void execute(Database db)  throws CommandException, DbException {

        db.deleteRecord(getParameter(Command.PARAM_ID));

        System.out.println("Deleted");
        System.out.println(Command.PARAM_ID + "=" + getParameter(Command.PARAM_ID));
        System.out.println();
    }
}

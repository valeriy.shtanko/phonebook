package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;
import com.shtanko.phonebook.model.Record;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:26 PM
 */
public class ReadCommand  extends Command {
    @Override
    public void execute(Database db)  throws CommandException, DbException {
        Record rec =  db.getRecord(getParameter(Command.PARAM_ID));

        if (rec != null) {
            System.out.println("Found");
            System.out.println(rec.toString());
            System.out.println();
        } else {
            System.out.println("Not found");
            System.out.println(Command.PARAM_ID + "=" + getParameter(Command.PARAM_ID));
            System.out.println();
        }
    }
}

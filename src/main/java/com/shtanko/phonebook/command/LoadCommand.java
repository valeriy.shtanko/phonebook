package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;
import com.shtanko.phonebook.model.Record;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: valeriy.shtanko
 * Date: 2/10/14
 * Time: 12:50 AM
 */
public class LoadCommand extends Command {
    @Override
    public void execute(Database db)  throws CommandException, DbException {
        String filePath = getParameter(Command.PARAM_PATH);

        try {
            ObjectInputStream stream = new ObjectInputStream(new FileInputStream(filePath));
            List<Record> data = (List<Record>)stream.readObject();

            for (Record record : data) {
                db.addRecord(record);
            }

            System.out.println("Loaded " + data.size() + " record(s)");
            System.out.println(Command.PARAM_PATH + "=" + getParameter(Command.PARAM_PATH));
            System.out.println();
        }
        catch (Exception e) {
            throw new DbException("Cannot read data from file '%s'. Error: %s", filePath,e.getMessage());
        }
    }
}

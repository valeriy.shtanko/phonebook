package com.shtanko.phonebook.command;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:28 PM
 */
public class CommandException extends Exception {
    public CommandException(String message) {
        super (message);
    }

    public CommandException(String format, Object... args) {
        super (String.format(format, args));
    }
}

package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;
import com.shtanko.phonebook.db.DbStatistics;

import java.util.Map;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:24 PM
 */
public abstract class Command {
    protected Map<String,String> parameters;

    //------------------------------------------------------------------------------------------------------------------
    // Public
    //
    public static String COMMAND_ADD      = "ADD";
    public static String COMMAND_UPDATE   = "UPDATE";
    public static String COMMAND_DELETE   = "DELETE";
    public static String COMMAND_READ     = "READ";
    public static String COMMAND_FLUSH    = "FLUSH";
    public static String COMMAND_LOAD     = "LOAD";
    public static String COMMAND_STAT     = "STAT";
    public static String COMMAND_COMMIT   = "COMMIT";
    public static String COMMAND_ROLLBACK = "ROLLBACK";

    public static String PARAM_ID         = "ID";
    public static String PARAM_NAME       = "NAME";
    public static String PARAM_PHONE      = "PHONE";
    public static String PARAM_PATH       = "PATH";

    public abstract void execute (Database db)  throws CommandException, DbException;

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public boolean isCommited(Database db) {
        DbStatistics stat = db.getStatistic();

        return (stat.getNotCommitedQuantity() == 0);
    }

    //------------------------------------------------------------------------------------------------------------------
    // Protected
    //
    protected String getParameter(String name) throws CommandException {
        if (this.parameters == null) {
            throw new CommandException("Parameters not assigned");
        }

        String result = parameters.get(name.trim());

        if (result == null) {
            throw new CommandException("Parameter '%s' not defined", name);
        }

        return result.trim();
    }
}

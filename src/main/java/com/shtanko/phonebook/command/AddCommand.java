package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;
import com.shtanko.phonebook.model.Record;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 8:25 PM
 */
public class AddCommand extends Command {

    @Override
    public void execute(Database db)  throws CommandException, DbException {
        Record rec = new Record();

        rec.setId(getParameter(Command.PARAM_ID));
        rec.setName(getParameter(Command.PARAM_NAME));
        rec.setPhone(getParameter(Command.PARAM_PHONE));

        db.addRecord(rec);

        System.out.println("Added");
        System.out.println(rec.toString());
        System.out.println();
    }
}

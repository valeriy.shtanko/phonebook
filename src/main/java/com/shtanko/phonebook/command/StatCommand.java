package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbStatistics;

/**
 * User: valeriy.shtanko
 * Date: 2/10/14
 * Time: 12:59 AM
 */
public class StatCommand extends Command {
    @Override
    public void execute(Database db)   {
        DbStatistics stat = db.getStatistic();

        System.out.println();
        System.out.println("-----------------------------");
        System.out.println("Total record(s) : " + stat.getTotalQuantity());
        System.out.println("-----------------------------");
        System.out.println("  Not committed : " + stat.getNotCommitedQuantity());
        System.out.println("       inserted : " + stat.getInsertedQuantity());
        System.out.println("        updated : " + stat.getUpdatedQuantity());
        System.out.println("        deleted : " + stat.getDeletedQuantity());
        System.out.println("-----------------------------");
        System.out.println();
    }
}

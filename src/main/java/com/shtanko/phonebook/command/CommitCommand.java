package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;

/**
 * User: valeriy.shtanko
 * Date: 2/12/14
 * Time: 1:34 AM
 */
public class CommitCommand  extends Command {
    @Override
    public void execute(Database db)   {
        int result = db.commit();

        System.out.println();
        System.out.println("Committed " + result + " record(s)");
        System.out.println();
    }
}

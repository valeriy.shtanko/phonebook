package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;
import com.shtanko.phonebook.db.DbException;
import com.shtanko.phonebook.model.Record;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * User: valeriy.shtanko
 * Date: 2/9/14
 * Time: 11:10 PM
 */
public class FlushCommand extends Command {
    @Override
    public void execute(Database db)  throws CommandException, DbException {
        List<Record> data = db.getAllRecords();

        if (!isCommited(db)){
            System.out.println("You have uncommitted record(s). Do 'commit' or 'rollback' first");
            System.out.println();
            return;
        }

        String filePath = getParameter(Command.PARAM_PATH);

        try {
            ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(filePath));

            stream.writeObject(data);
            stream.flush();
            stream.close();

            System.out.println("Saved " + data.size() + " record(s)");
            System.out.println(Command.PARAM_PATH + "=" + getParameter(Command.PARAM_PATH));
            System.out.println();
        }
        catch (Exception e) {
            throw new DbException("Cannot write data to file '%s'. Error: %s", filePath,e.getMessage());
        }
    }
}

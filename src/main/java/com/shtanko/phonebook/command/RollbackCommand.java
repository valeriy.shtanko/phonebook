package com.shtanko.phonebook.command;

import com.shtanko.phonebook.db.Database;

/**
 * User: valeriy.shtanko
 * Date: 2/12/14
 * Time: 1:53 AM
 */
public class RollbackCommand  extends Command {
    @Override
    public void execute(Database db)   {
        int result = db.rollback();

        System.out.println();
        System.out.println("Rolled back " + result + " record(s)");
        System.out.println();
    }
}

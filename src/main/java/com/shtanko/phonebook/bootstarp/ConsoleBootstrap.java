package com.shtanko.phonebook.bootstarp;

import com.shtanko.phonebook.command.CommandParser;
import com.shtanko.phonebook.db.Database;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class ConsoleBootstrap
{
    public static void main( String[] args ) {
        System.out.println();
        System.out.println("Phone Book DB started");
        printHelp();

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.print("DB> ");
            String commandLine = sc.nextLine();

            if (commandLine.trim().equals("")) {
                continue;
            }

            // Console commands
            if (commandLine.toUpperCase().equals("EXIT")) {
                break;
            }

            if (commandLine.toUpperCase().equals("HELP") || commandLine.toUpperCase().equals("?")) {
                printHelp();
                continue;
            }

            // Db Commands
            try {
                CommandParser.getInstance()
                             .parse(commandLine)
                             .execute(Database.getInstance());
            }
            catch (Exception e) {
              System.out.println(e.getMessage());
              System.out.println();
          }
        }

        sc.close();
        System.out.println( "Phone Book DB stopped" );
    }

    //------------------------------------------------------------------------------------------------------------------
    // Protected
    //
    private static void printHelp() {
        System.out.println();
        System.out.println("Usage:");
        System.out.println("  add    -id=<id> -name=<name> -phone=<phone>");
        System.out.println("  delete -id=<id>");
        System.out.println("  update -id=<id> -name=<name> -phone=<phone>");
        System.out.println("  read   -id=<id>");
        System.out.println("  flush  -path=<filePath>");
        System.out.println("  load   -path=<filePath>");
        System.out.println("  commit");
        System.out.println("  rollback");
        System.out.println("  stat");
        System.out.println("  help");
        System.out.println("  exit");
        System.out.println();
    }
}

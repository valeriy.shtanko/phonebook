package com.shtanko.phonebook;

import com.shtanko.phonebook.command.CommandParser;
import com.shtanko.phonebook.db.Database;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple ConsoleBootstrap.
 */
public class SaveLoadTest extends TestCase
{
    public SaveLoadTest(String testName) {
        super( testName );
    }

    public static Test suite() {
        return new TestSuite( SaveLoadTest.class );
    }

    public void prepareData() throws Exception
    {
        CommandUtil.executeCommand("add -id=ivanov  -name=invanov -phone=invanov_phone");
        CommandUtil.executeCommand("add -id=petrov  -name=petrov  -phone=petrov_phone");
        CommandUtil.executeCommand("add -id=sidorov -name=sidorov -phone=sidorov_phone");

        CommandUtil.executeCommand("commit");
        CommandUtil.printDbStatistics();
    }

    public void testSaveLoad() throws Exception
    {
        prepareData();
        String filePath = "c:/db.dat";

        CommandUtil.executeCommand("flush  -path=" + filePath);

        // Remove ald data
        CommandUtil.executeCommand("delete -id=ivanov");
        CommandUtil.executeCommand("delete -id=petrov");
        CommandUtil.executeCommand("delete -id=sidorov");
        CommandUtil.executeCommand("commit");

        // print
        CommandUtil.executeCommand("load   -path=" + filePath);
        CommandUtil.executeCommand("commit");
        CommandUtil.printDbStatistics();
    }
}

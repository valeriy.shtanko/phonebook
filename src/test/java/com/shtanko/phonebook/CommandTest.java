package com.shtanko.phonebook;

import com.shtanko.phonebook.command.CommandParser;
import com.shtanko.phonebook.db.Database;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple ConsoleBootstrap.
 */
public class CommandTest extends TestCase
{
    public CommandTest(String testName) {
        super( testName );
    }

    public static Test suite() {
        return new TestSuite( CommandTest.class );
    }

    public void testStatistics() throws Exception
    {
        CommandUtil.executeCommand("add -id=ivanov  -name=invanov -phone=invanov_phone");
        CommandUtil.executeCommand("add -id=petrov  -name=petrov  -phone=petrov_phone");
        CommandUtil.executeCommand("add -id=sidorov -name=sidorov -phone=sidorov_phone");

        CommandUtil.printDbStatistics();

        CommandUtil.executeCommand("update -id=sidorov -name=xxxx -phone=eeee");

        CommandUtil.printDbStatistics();

        CommandUtil.executeCommand("commit");

        CommandUtil.printDbStatistics();
    }
}

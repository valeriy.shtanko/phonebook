package com.shtanko.phonebook;

import com.shtanko.phonebook.command.CommandParser;
import com.shtanko.phonebook.db.Database;

/**
 * @author: valeriy.shtanko
 * created: 2014-02-12 10:57
 */
public class CommandUtil {
    public static void executeCommand(String command) throws Exception {
        System.out.println("DB> " + command);
        CommandParser.getInstance()
                .parse(command)
                .execute(Database.getInstance());
    }

    public static void printDbStatistics()  throws Exception {
        executeCommand("stat");
    }
}
